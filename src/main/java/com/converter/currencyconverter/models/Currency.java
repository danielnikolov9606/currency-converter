package com.converter.currencyconverter.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "currencies")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "unit")
    private String units;

    @Column(name = "value_in_bgn")
    private String valueBGN;

    @Column(name = "inverse_rate")
    private String inverseRate;

    public Currency(int id,
                    String name,
                    String code,
                    String units,
                    String valueBGN,
                    String inverseRate) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.units = units;
        this.valueBGN = valueBGN;
        this.inverseRate = inverseRate;
    }

    public Currency() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getValueBGN() {
        return valueBGN;
    }

    public void setValueBGN(String valueBGN) {
        this.valueBGN = valueBGN;
    }

    public String getInverseRate() {
        return inverseRate;
    }

    public void setInverseRate(String inverseRate) {
        this.inverseRate = inverseRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return id == currency.id
                && name.equals(currency.name)
                && code.equals(currency.code)
                && units.equals(currency.units)
                && valueBGN.equals(currency.valueBGN)
                && inverseRate.equals(currency.inverseRate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, units, valueBGN, inverseRate);
    }
}
