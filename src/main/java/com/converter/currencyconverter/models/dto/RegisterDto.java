package com.converter.currencyconverter.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.converter.currencyconverter.utils.GlobalConstants.*;

public class RegisterDto {

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_FIRST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String firstName;

    @Size(min = MIN_USER_NAME_LENGTH, max = MAX_USER_NAME_LENGTH, message = USER_LAST_NAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String lastName;

    @Size(min = MIN_USER_USERNAME_LENGTH, max = MAX_USER_USERNAME_LENGTH, message = USER_USERNAME_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String username;

    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = USER_PASSWORD_LENGTH_RANGE_MESSAGE)
    @NotBlank
    private String password;

    @NotBlank
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = USER_PASSWORD_LENGTH_RANGE_MESSAGE)
    private String repeatPassword;


    public RegisterDto() {
    }

    public RegisterDto(String firstName,
                       String lastName,
                       String username,
                       String password,
                       String repeatPassword) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.repeatPassword = repeatPassword;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
