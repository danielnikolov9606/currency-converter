package com.converter.currencyconverter.models.dto;

import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

import static com.converter.currencyconverter.utils.GlobalConstants.*;


public class CurrencyDto {

    @NotNull
    @Size(min = MIN_CURRENCY_NAME_LENGTH, max = MAX_CURRENCY_NAME_LENGTH, message = CURRENCY_NAME_LENGTH_RANGE_MESSAGE)
    private String name;

    @NotNull
    @Size(min = MIN_CURRENCY_CODE_LENGTH, max = MAX_CURRENCY_CODE_LENGTH, message = CURRENCY_CODE_LENGTH_RANGE_MESSAGE)
    private String code;

    @NotNull
    private String units;

    @NotNull
    private String valueBGN;

    @NotNull
    private String inverseRate;

    public CurrencyDto() {
    }

    public CurrencyDto(String name, String code, String units, String valueBGN, String inverseRate) {
        this.name = name;
        this.code = code;
        this.units = units;
        this.valueBGN = valueBGN;
        this.inverseRate = inverseRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getValueBGN() {
        return valueBGN;
    }

    public void setValueBGN(String valueBGN) {
        this.valueBGN = valueBGN;
    }

    public String getInverseRate() {
        return inverseRate;
    }

    public void setInverseRate(String inverseRate) {
        this.inverseRate = inverseRate;
    }
}
