package com.converter.currencyconverter.repositories.contracts;

public interface CreateRepository<T> {

    void create(T entityToCreate);
}