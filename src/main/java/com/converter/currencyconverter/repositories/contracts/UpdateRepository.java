package com.converter.currencyconverter.repositories.contracts;

public interface UpdateRepository<T> {

    void update(T entityToUpdate);
}
