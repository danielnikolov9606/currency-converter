package com.converter.currencyconverter.repositories.contracts;

import com.converter.currencyconverter.models.Currency;

public interface CurrencyRepository extends GetRepository<Currency>, CreateRepository<Currency>, UpdateRepository<Currency>{

    Currency getByName(String name);

    Currency getByCode(String code);

    boolean checkIfCurrencyExists(String code);

}
