package com.converter.currencyconverter.repositories.contracts;

import com.converter.currencyconverter.models.User;


public interface UserRepository extends GetRepository<User>, CreateRepository<User>{

    User getByUsername(String username);

}
