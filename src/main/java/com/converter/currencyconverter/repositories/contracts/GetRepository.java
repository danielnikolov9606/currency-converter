package com.converter.currencyconverter.repositories.contracts;

import java.util.List;

public interface GetRepository<T> {

    List<T> getAll();

    T getById(int id);
}
