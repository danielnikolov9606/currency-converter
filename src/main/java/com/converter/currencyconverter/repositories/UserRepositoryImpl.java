package com.converter.currencyconverter.repositories;

import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.repositories.contracts.AbstractGenericGetRepository;
import com.converter.currencyconverter.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl extends AbstractGenericGetRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        return super.getAll(User.class);
    }

    @Override
    public User getById(int id) {
        return super.getByField("id", id, User.class);
    }

    @Override
    public User getByUsername(String username) {
        return super.getByField("username", username, User.class);
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }
}
