package com.converter.currencyconverter.repositories;

import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.repositories.contracts.AbstractGenericGetRepository;
import com.converter.currencyconverter.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl extends AbstractGenericGetRepository<Currency> implements CurrencyRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory, SessionFactory sessionFactory1) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory1;
    }

    @Override
    public List<Currency> getAll() {
        return super.getAll(Currency.class);
    }

    @Override
    public Currency getById(int id) {
        return super.getByField("id", id, Currency.class);
    }

    @Override
    public Currency getByName(String name) {
        return super.getByField("name", name, Currency.class);
    }

    @Override
    public Currency getByCode(String code) {
        return super.getByField("code", code, Currency.class);
    }

    @Override
    public void create(Currency entityToCreate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(entityToCreate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Currency entityToUpdate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(entityToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkIfCurrencyExists(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("From Currency WHERE code = :code", Currency.class);
            query.setParameter("code", code);
            List<Currency> result = query.list();
            return !result.isEmpty();
        }
    }

}
