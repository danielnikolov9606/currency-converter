package com.converter.currencyconverter.controllers.mvc;

import com.converter.currencyconverter.controllers.AuthenticationHelper;
import com.converter.currencyconverter.controllers.mappers.CurrencyModelMapper;
import com.converter.currencyconverter.exceptions.DuplicateEntityException;
import com.converter.currencyconverter.exceptions.UnauthorizedOperationException;
import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.models.dto.ConvertDto;
import com.converter.currencyconverter.models.dto.CurrencyDto;
import com.converter.currencyconverter.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CurrencyService currencyService;
    private final CurrencyModelMapper currencyModelMapper;


    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper,
                             CurrencyService currencyService,
                             CurrencyModelMapper currencyModelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.currencyService = currencyService;
        this.currencyModelMapper = currencyModelMapper;
    }

    @ModelAttribute("currencies")
    public List<Currency> populateCurrencies() {
        return currencyService.getAll();
    }

    @GetMapping
    public String showHomepage(Model model, HttpSession session) {
        model.addAttribute("convertDto", new ConvertDto());
        return "index";
    }

    @PostMapping()
    public String convert(@Valid @ModelAttribute("convert") ConvertDto convertDto,
                          Model model, HttpSession session) {
        try {
            String result = currencyService.convert(
                    convertDto.getCurrencyFromId(),
                    convertDto.getCurrencyToId(),
                    convertDto.getAmount());
            model.addAttribute("result", result);
        } catch (NumberFormatException e) {
            model.addAttribute("error", e.getMessage());
            return "errors/404";
        }

        model.addAttribute("convertDto", convertDto);
        return "index";
    }

    @GetMapping("/create")
    public String showCreateCurrency(Model model, HttpSession session) {

        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        }
        CurrencyDto currencyDto = new CurrencyDto();
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("currencyDto", currencyDto);
        return "currency-create";
    }

    @PostMapping("/create")
    public String handleCreateCurrency(@Valid @ModelAttribute("currencyDto") CurrencyDto currencyDto,
                                       BindingResult bindingResult, Model model,
                                       HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "currency-create";
        }

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            Currency currency = currencyModelMapper.currencyFromDto(currencyDto);
            currencyService.createCurrency(currency);
        } catch (UnauthorizedOperationException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/403";
        } catch (DuplicateEntityException | NumberFormatException exc) {
            model.addAttribute("error", exc.getMessage());
            return "errors/404";
        }

        return "redirect:/";
    }


}