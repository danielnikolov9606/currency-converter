package com.converter.currencyconverter.controllers.rest;


import com.converter.currencyconverter.controllers.AuthenticationHelper;
import com.converter.currencyconverter.controllers.mappers.CurrencyModelMapper;
import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.models.dto.CurrencyDto;
import com.converter.currencyconverter.services.WebScrape;
import com.converter.currencyconverter.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/currencyconverter/currencies")
public class CurrencyController {

    private final CurrencyService currencyService;
    private final CurrencyModelMapper currencyModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WebScrape webScrape;

    @Autowired
    public CurrencyController(CurrencyService currencyService,
                              CurrencyModelMapper currencyModelMapper,
                              AuthenticationHelper authenticationHelper, WebScrape webScrape) {
        this.currencyService = currencyService;
        this.currencyModelMapper = currencyModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.webScrape = webScrape;
    }

    @GetMapping
    public List<Currency> getAll() {
        return currencyService.getAll();
    }

    @GetMapping("/{id}")
    public Currency getById(@PathVariable int id) {
        return currencyService.getById(id);
    }

    @PostMapping()
    public Currency createCurrency(@Valid @RequestBody CurrencyDto currencyDto,  @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        Currency currency = currencyModelMapper.currencyFromDto(currencyDto);
        currencyService.createCurrency(currency);
        return currency;
    }

    @GetMapping("{currencyFromId}/convertTo/{currencyToId}")
    public String convert(@PathVariable int currencyFromId,
                          @PathVariable int currencyToId,
                          @RequestParam String amount) {
        return currencyService.convert(currencyFromId, currencyToId, amount);
    }


}
