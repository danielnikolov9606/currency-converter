package com.converter.currencyconverter.controllers.rest;

import com.converter.currencyconverter.controllers.mappers.UserModelMapper;
import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.models.dto.UserDto;
import com.converter.currencyconverter.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/currencyconverter/users")
public class UserController {

    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public UserController(UserService userService, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return userService.getById(id);
    }

    @PostMapping()
    public User createUser(@Valid @RequestBody UserDto userDto) {
        User user = userModelMapper.userFromDto(userDto);
        userService.createUser(user);
        return user;
    }

}
