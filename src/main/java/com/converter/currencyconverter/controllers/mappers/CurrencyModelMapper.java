package com.converter.currencyconverter.controllers.mappers;

import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.models.dto.CurrencyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrencyModelMapper {
    @Autowired
    public CurrencyModelMapper() {
    }

    public Currency currencyFromDto(CurrencyDto currencyDto) {
        Currency currency = new Currency();
        dtoToObject(currencyDto, currency);
        return currency;
    }

    public void dtoToObject(CurrencyDto currencyDto, Currency currency){
        currency.setName(currencyDto.getName());
        currency.setCode(currencyDto.getCode());
        currency.setUnits(currencyDto.getUnits());
        currency.setValueBGN(currencyDto.getValueBGN());
        currency.setInverseRate(currencyDto.getInverseRate());
    }

}
