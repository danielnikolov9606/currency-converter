package com.converter.currencyconverter.controllers.mappers;

import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.models.dto.RegisterDto;
import com.converter.currencyconverter.models.dto.UserDto;
import com.converter.currencyconverter.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserRepository userRepository;
    @Autowired
    public UserModelMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public User userFromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public void dtoToObject(UserDto userDto, User user){
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
    }

    public User userFromDto(RegisterDto registerDto) {
        User user = new User();
        registerDtoToObject(registerDto, user);
        return user;
    }

    private void registerDtoToObject(RegisterDto registerDto, User user) {
        user.setUsername(registerDto.getUsername());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setPassword(registerDto.getPassword());
    }

}
