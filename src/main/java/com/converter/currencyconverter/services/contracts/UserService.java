package com.converter.currencyconverter.services.contracts;

import com.converter.currencyconverter.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    void createUser(User user);

}
