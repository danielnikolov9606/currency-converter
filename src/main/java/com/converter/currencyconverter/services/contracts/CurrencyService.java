package com.converter.currencyconverter.services.contracts;

import com.converter.currencyconverter.models.Currency;

import java.util.List;

public interface CurrencyService {

    List<Currency> getAll();

    Currency getById(int id);

    Currency getByName(String name);

    Currency getByCode(String code);

    void createCurrency(Currency currency);

    void updateCurrency(Currency currency);

    String convert(int currencyFromId, int currencyToId, String amount);

    boolean checkIfCurrencyExists(String code);

}
