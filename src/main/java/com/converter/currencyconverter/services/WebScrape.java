package com.converter.currencyconverter.services;

import com.converter.currencyconverter.exceptions.EntityNotFoundException;
import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.services.contracts.CurrencyService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class WebScrape {

    public final CurrencyService currencyService;

    @Autowired
    public WebScrape(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    //Runs every 24 hours
    @Scheduled(fixedRate = 24 * 1000 * 60 * 60)
    public void scrapeDataFromWeb() {
        String url = "https://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm";
        Document document;
        try {
            document = Jsoup.connect(url).userAgent("mozilla/17.0").get();
            Elements elements = document.select("tr");
            updateDataBase(elements);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDataBase(Elements elements) {
        for (Element element : elements) {
            if (element.select("td").text().equals("")) {
                continue;
            } else {
                if (currencyService.checkIfCurrencyExists(element.getElementsByTag("td").get(1).text())) {
                    Currency currency = currencyService.getByCode(element.getElementsByTag("td").get(1).text());

                    currency.setUnits(element.getElementsByTag("td").get(2).text().replace(" ", ""));
                    currency.setValueBGN(element.getElementsByTag("td").get(3).text().replace(" ", ""));
                    currency.setInverseRate(element.getElementsByTag("td").get(4).text().replace(" ", ""));

                    currencyService.updateCurrency(currency);
                }
            }
        }
    }
}
