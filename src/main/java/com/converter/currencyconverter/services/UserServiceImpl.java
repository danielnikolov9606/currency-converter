package com.converter.currencyconverter.services;

import com.converter.currencyconverter.exceptions.DuplicateEntityException;
import com.converter.currencyconverter.exceptions.EntityNotFoundException;
import com.converter.currencyconverter.models.User;
import com.converter.currencyconverter.repositories.contracts.UserRepository;
import com.converter.currencyconverter.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.converter.currencyconverter.utils.GlobalConstants.USER_ALREADY_EXISTS;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void createUser(User user) {
        checkDuplicateUser(user);
        userRepository.create(user);
    }

    private void checkDuplicateUser(User user) {
        boolean duplicateExists = true;

        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(USER_ALREADY_EXISTS);
        }
    }

}
