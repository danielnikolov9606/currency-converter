package com.converter.currencyconverter.services;

import com.converter.currencyconverter.exceptions.DuplicateEntityException;
import com.converter.currencyconverter.exceptions.EntityNotFoundException;
import com.converter.currencyconverter.models.Currency;
import com.converter.currencyconverter.repositories.contracts.CurrencyRepository;
import com.converter.currencyconverter.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static com.converter.currencyconverter.utils.GlobalConstants.*;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<Currency> getAll() {
        return currencyRepository.getAll();
    }

    @Override
    public Currency getById(int id) {
        return currencyRepository.getById(id);
    }

    @Override
    public Currency getByName(String name) {
        return currencyRepository.getByName(name);
    }

    @Override
    public Currency getByCode(String code) {
        return currencyRepository.getByCode(code);
    }

    @Override
    public void createCurrency(Currency currency) {
        checkDuplicateCurrencyName(currency);
        checkDuplicateCurrencyCode(currency);
        validateCurrencyValues(currency);
        currencyRepository.create(currency);
    }

    @Override
    public void updateCurrency(Currency currency) {
        currencyRepository.update(currency);
    }

    @Override
    public String convert(int currencyFromId, int currencyToId, String amount) {
        try {
            BigDecimal amountToNumber = new BigDecimal(amount);
            Currency currencyFrom = currencyRepository.getById(currencyFromId);
            BigDecimal currencyFromValue = new BigDecimal(currencyFrom.getValueBGN());
            BigDecimal currencyFromUnits= new BigDecimal(currencyFrom.getUnits());
            Currency currencyTo = currencyRepository.getById(currencyToId);
            BigDecimal currencyToValue = new BigDecimal(currencyTo.getInverseRate());
            BigDecimal convertResult = amountToNumber
                    .multiply(currencyFromValue)
                    .multiply(currencyToValue)
                    .divide(currencyFromUnits, 6, RoundingMode.HALF_UP);
            return convertResult.toString();
        } catch (Exception e){
            throw new NumberFormatException(INVALID_VALUES_PASSED);
        }
    }

    @Override
    public boolean checkIfCurrencyExists(String code) {
        return currencyRepository.checkIfCurrencyExists(code);
    }


    private void checkDuplicateCurrencyName(Currency currency) {
        boolean duplicateExists = true;

        try {
            currencyRepository.getByName(currency.getName());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(CURRENCY_ALREADY_EXISTS);
        }
    }

    private void checkDuplicateCurrencyCode(Currency currency) {
        boolean duplicateExists = true;

        try {
            currencyRepository.getByCode(currency.getCode());
        } catch (EntityNotFoundException exc) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(CURRENCY_CODE_ALREADY_EXISTS);
        }
    }

    private void validateCurrencyValues(Currency currency) {
        BigDecimal values;
        try{
            values = new BigDecimal(currency.getUnits());
            checkIfValuesArePositive(values);
            values = new BigDecimal(currency.getValueBGN());
            checkIfValuesArePositive(values);
            values = new BigDecimal(currency.getInverseRate());
            checkIfValuesArePositive(values);
        } catch (Exception e){
            throw new NumberFormatException(INVALID_VALUES_PASSED);
        }
    }

    private void checkIfValuesArePositive(BigDecimal values) {
        if (values.compareTo(new BigDecimal("0")) < 0){
            throw new NumberFormatException();
        }
    }

}
