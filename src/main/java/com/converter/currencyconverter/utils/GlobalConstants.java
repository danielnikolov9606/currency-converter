package com.converter.currencyconverter.utils;

public class GlobalConstants {

    // Messages
    public static final String USER_LAST_NAME_LENGTH_RANGE_MESSAGE = "User's last name should be between 2 and 50 symbols.";
    public static final String USER_FIRST_NAME_LENGTH_RANGE_MESSAGE = "User's first name should be between 2 and 50 symbols.";
    public static final String USER_PASSWORD_LENGTH_RANGE_MESSAGE = "User's password should be between 8 and 50 symbols.";
    public static final String USER_USERNAME_LENGTH_RANGE_MESSAGE = "User's username should be between 4 and 50 symbols.";
    public static final String CURRENCY_NAME_LENGTH_RANGE_MESSAGE = "Currency name should be between 2 and 50 symbols";
    public static final String CURRENCY_CODE_LENGTH_RANGE_MESSAGE = "Currency code should be between 2 and 10 symbols";

    // Range Min-Max
    public static final int MIN_USER_USERNAME_LENGTH = 4;
    public static final int MAX_USER_USERNAME_LENGTH = 50;
    public static final int MIN_USER_NAME_LENGTH = 2;
    public static final int MAX_USER_NAME_LENGTH = 50;
    public static final int MIN_PASSWORD_LENGTH = 8;
    public static final int MAX_PASSWORD_LENGTH = 50;
    public static final int MIN_CURRENCY_NAME_LENGTH = 2;
    public static final int MAX_CURRENCY_NAME_LENGTH = 50;
    public static final int MIN_CURRENCY_CODE_LENGTH = 2;
    public static final int MAX_CURRENCY_CODE_LENGTH = 10;

    // Authorization
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String WRONG_USERNAME_PASSWORD = "Wrong username/password.";
    public static final String NO_USER_LOGGED = "No logged in user.";

    // Duplication
    public static final String USER_ALREADY_EXISTS = "User already exists";
    public static final String CURRENCY_ALREADY_EXISTS = "Currency with this name already exists";
    public static final String CURRENCY_CODE_ALREADY_EXISTS = "Currency with this code already exists";

    // Not Found
    public static final String NO_RESULTS_FOUND = "No results found";


    //Exceptions
    public static final String INVALID_VALUES_PASSED = "The values are either negative or contain invalid characters.";

}
