create database `currency_converter`; 

use `currency_converter`;

create or replace table currencies
(
	currency_id int auto_increment
		primary key,
	name varchar(50) not null,
	code varchar(10) not null,
	unit text not null,
	value_in_bgn text not null,
	inverse_rate text not null,
	constraint currencies_code_uindex
		unique (code),
	constraint currencies_name_uindex
		unique (name)
);

create or replace table users
(
	user_id int auto_increment
		primary key,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	username varchar(50) not null,
	password varchar(30) not null,
	constraint users_username_uindex
		unique (username)
);
